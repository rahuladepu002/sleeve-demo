// import logo from './logo.svg';
import './App.css';
import Customization from './components/customization/Customization';
import Sleeve from './components/sleeve/Sleeve';
import Theme from './components/theme/Theme';
function App() {
  return (
    <div>
      <Sleeve />
      <Theme />
      <Customization />

    </div>
  );
}

export default App;
