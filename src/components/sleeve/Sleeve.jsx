import React from 'react'
import './sleeve.css'
import appmusic from '../../images/AppMusic.png'
import spotify from '../../images/AppSpotify.png'
import doppler from '../../images/AppDoppler.png'
import shelfcallout from '../../images/ShelfCallout.png'
function Sleeve() {
  return (
    <div className='container'>
      <h1 id='sleeve'>Sleeve 2</h1>
      <h6>The ultimate music accessory for your Mac</h6>
      <p> Sleeve sits on the desktop, displaying and controlling the music you’re <br/>currently playing in
        
        <img className='img' src={appmusic} /> Apple Music,
        
        <img className='img' src={spotify} />Spotify, and
        <img className='img' src={doppler} />Doppler.</p>
      <div>
        <div id='btn'>
          <button id='btn-1'>Mac Apple Store</button>
          <button id='btn-2'>Buy Directly</button>
        </div>
        <pre>
          No subscriptions. No in-app purchases.<br />
          Requires macOS 11 Big Sur or later.
        </pre>
        <div id='input'><img className='shelfimg' src={shelfcallout} /><p id='shelfp'>Now with shelves and a progress bar.See what's new in Sleeve 2.3</p></div>
      </div>
    </div>
  )
}

export default Sleeve
