import React, { useEffect, useState } from 'react'
import './custom.css'
function Customization() {

  let [isImage, setIsImage] = useState(false);


  useEffect(() => {
    window.addEventListener("scroll", (e) => {
      if (window.pageYOffset == 1900) {
        setIsImage(true);
      }
    })

    return () => {
      window.removeEventListener("scroll", (e) => {

      })
    }

  }, [])
  return (

    <div id='custom'>
      <h5 >CUSTOMIZATION</h5>
      <h2>Countless ways to customize.</h2>
      <p>Customization is at the core of the Sleeve experience — choose from any <br /> combination of design choices, behaviors and settings to make Sleeve at home on<br /> your desktop.</p>

      <div id='green-box'>

      </div>

      <div id='art-box'>
        <div id='art'>
          <h1>Art Work</h1>
          <p>Scale artwork all the way up or all the way down.
            Round the corners or leave them square.

            <br />Choose shadow and lighting effects to bring your album artwork to life.

            Or hide it completely.</p>
        </div>



        <div style={{ width: isImage ? "500px" : "100px" }} id='image'><img src='https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' alt='peacock' /></div>


      </div>

      <div id='art-box'>
        <div id='art'>
          <h1>Art Work</h1>
          <p>Scale artwork all the way up or all the way down.
            Round the corners or leave them square.

            <br />Choose shadow and lighting effects to bring your album artwork to life.

            Or hide it completely.</p>
        </div>



        <div id='image'><img src='https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' alt='peacock' /></div>


      </div>

      <div id='art-box'>
        <div id='art'>
          <h1>Art Work</h1>
          <p>Scale artwork all the way up or all the way down.
            Round the corners or leave them square.

            <br />Choose shadow and lighting effects to bring your album artwork to life.

            Or hide it completely.</p>
        </div>



        <div id='image'><img src='https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' alt='peacock' /></div>


      </div>

      <div id='art-box'>
        <div id='art'>
          <h1>Art Work</h1>
          <p>Scale artwork all the way up or all the way down.
            Round the corners or leave them square.

            Choose shadow and lighting effects to bring your album artwork to life.

            Or hide it completely.</p>
        </div>


        <div id='image'><img src='https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' alt='peacock' /></div>




      </div>


    </div>

    // <div class="outer-container">
    //   <div class="container">
    //     <div class="content">
    //       {/* <!-- Your main content here --> */}
    //       <p>Scroll down to see the sticky effect.</p>
    //       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lacinia odio vitae vestibulum vestibulum.</p>
    //       <p>Cras vehicula, dolor a commodo suscipit, felis neque cursus urna, ut auctor orci elit ut urna.</p>
    //       {/* <!-- Add more content here for demonstration --> */}
    //       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lacinia odio vitae vestibulum vestibulum.</p>
    //       <p>Cras vehicula, dolor a commodo suscipit, felis neque cursus urna, ut auctor orci elit ut urna.</p>
    //       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lacinia odio vitae vestibulum vestibulum.</p>
    //       <p>Cras vehicula, dolor a commodo suscipit, felis neque cursus urna, ut auctor orci elit ut urna.</p>
    //     </div>
    //     <div class="green-div">
    //       <p>Sticky Green Div</p>
    //     </div>
    //   </div>
    // {/* </div> */}
  )
}

export default Customization
