import React from 'react'
import './theme.css'
import theme from '../../images/themes.png'
function Theme() {
  return (
    <div id='theme'>
      <h3>NEW IN 2.0</h3>
          <h6 id='themesh6'>Themes. Unlimited themes.</h6>
      <p id='themesp'>Themes in Sleeve make creating and switching between customizations easy. Share your own creations with friends and install as many themes as you like with just a double-click.</p>
      <img id='themeimg' src={theme} alt='themes' />
    </div>
  )
}

export default Theme
